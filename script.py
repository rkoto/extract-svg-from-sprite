from pathlib import Path
import os

savePath = 'icons'
rootFile = Path('sprite.svg').open().read()

def getSVGSymbols():
    start = rootFile.split('<svg>')[1].replace('\n\t', '').replace('\n', '').replace('\t', '')
    end = start.split('</svg>')[0]
    return end.split('<symbol')

def filterEmptyArrayItems (items):
    return list(filter(lambda x: len(x), items))

def updateSVGSymbols():
    symbols = getSVGSymbols()
    filteredArray = filterEmptyArrayItems(symbols)
    return list(map(lambda x: '<symbol' + x, filteredArray))

def formatItem(item):
    return item.replace('symbol', 'svg')

def formatSymbolToSVG():
    updatedList = updateSVGSymbols()
    return list(map(lambda x: formatItem(x), updatedList))

def saveExtractedSVGSymbol():
    svgList = formatSymbolToSVG()
    
    try:
        os.mkdir(savePath)
        print('Directory ' + savePath + ' created.')
    except FileExistsError:
        print('Directory ' + savePath + ' already exist - skipped')
    
    for (index, item) in enumerate(svgList):
        num = str(index)
        svg = open(savePath + '/svg-' + num + '.svg', 'w')
        svg.write(item)
        svg.close()
        print('File svg-' + num + '.svg saved.')

saveExtractedSVGSymbol()